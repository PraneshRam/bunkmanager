/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author siva1
 */
@WebServlet(name = "Bunky", urlPatterns = {"/Bunky"})
public class Bunky extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Bunky</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Bunky at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       // processRequest(request, response);
           
    
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       //processRequest(request, response);
       JSONObject main=new JSONObject();
       String roll=request.getParameter("roll");
       String pass=request.getParameter("pass");
String userAgent = "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0";
        Jsoup.connect("https://ecampus.psgtech.ac.in/studzone/").timeout(10*1000).get(); 

        Connection.Response res = Jsoup.connect("https://ecampus.psgtech.ac.in/studzone/")
                .data("btnlogin", "Login")
                .data("TxtPasswd", pass)
                .data("Txtstudid", roll)
                .data("__EVENTTARGET","")
                .data("__EVENTARGUMENT", "")
                .data("__VIEWSTATE", "xmweAl6PUUEBu++dLiGINppqOEbgfCwFkxo/oNMJaaS6Orcvpel/FOmEcdso7/VtKYVnWdyRjhA9N+fJkUSxbSB6qd/YdynaMNN08LmjFlmAFdN0LaTHM0ihcvnbk3qQlnQY/PlrabescMBW0ReXxl7XtXXaHHpJM9RKS1YYqlotqvkcI7q1nwHVz1/7b9AC5o6fIO1WKcEA5TA0Fd/vr+3qv6q1agCxQxGh/Tjo5do=")
                .data("__EVENTVALIDATION", "fae+sUTwfV3QyImy+Mg4y7Nn5PYAupQWVuZMcP3oFPh2tTJIGjBfHmMifAAPLYoMMEPmwj4jfxRv/qdXbv1HGS+EqYC42C9R8mwKWAe25WQQkS4Y5sZE+6Sdnri2bT7r4AbdQFXeKHfi/a79etTEdvZmr5Hq+cbgddj4vyQFeaB+vOGlBfJdy352G9gRdbw7c5dj9BM6P/yhzmyplqzk5ffLaa3fB8XpzuEeXIOTLXFcCP9o/QsgJke55G3sgdWRRutRQ0qZ4b3tD8uJ144zmw==")
                .data("__LASTFOCUS","")
                .data("rdolst","S")
                .userAgent(userAgent)
                .method(Connection.Method.POST)
                .followRedirects(false)
                .execute();
        
        // System.out.println(res.cookies());
        //org.jsoup.nodes.Document doc=res.parse();
        Connection.Response resp = Jsoup.connect("https://ecampus.psgtech.ac.in/studzone/AttWfStudMenu.aspx")
                .cookies(res.cookies())
                .userAgent(userAgent)
                .method(Connection.Method.POST)
                .followRedirects(false)
                .execute();
        
        Connection.Response resp1 = Jsoup.connect("https://ecampus.psgtech.ac.in/studzone/AttWfPercView.aspx")
                .cookies(res.cookies())
                .userAgent(userAgent)
                .method(Connection.Method.POST)
                .followRedirects(false)
                .execute();
        
       // System.out.println(resp1);
        org.jsoup.nodes.Document d=resp1.parse();
        //System.out.println(d);
        response.setContentType("json");
     PrintWriter out=response.getWriter();
        if(!d.title().equals("Object moved")){
        Element date = d.select("table").get(5);
        Elements rows=date.select("tr");
       // System.out.println(rows.size());
        for(int i=0; i<rows.size(); i++){
            Element d1=rows.get(i);
            Elements cols=d1.select("span");
            for(int j=0;j<cols.size();j++){
               Element col=cols.get(j);
                 //System.out.println(col.text());
                 main.put("date",col.text());
            }
        }
        
      Element name=d.select("table").get(6);
      Elements rows1=name.select("tr");
      //System.out.println(rows1.size());
      ArrayList<String>values=new ArrayList<String>();
      for(int i=0; i<rows1.size(); i++){
          Element d1=rows1.get(i);
          Elements cols=d1.select("td");
         // System.out.println(cols.size());
          for(int j=0; j<cols.size(); j++){
              Element col=cols.get(j);
              if(!col.text().equals(":"))
                 values.add(col.text());
          }
      }
      if(values.size()%2==0){
          for(int p=0;p<values.size();){
              main.put(values.get(p).replaceAll("\\s", ""),values.get(p+1));
              p=p+2;
          }
      }
        
        Element table = d.select("table").get(9);
        Elements rows2 = table.select("tr");
        Element row = rows2.get(1);
        Elements cols=row.select("td");
        ArrayList<String>title=new ArrayList<String>();
        for(int i=0;i<cols.size();i++){
            System.out.println("ji");
            Element col=cols.get(i);
            title.add(col.text());
        }
        JSONArray ja=new JSONArray();
        for (int i = 2; i < rows2.size(); i++) {
            System.out.println("jll");
            Element row1 = rows2.get(i);
            Elements cols1=row1.select("td");
            JSONObject jb=new JSONObject();
            
            for(int j=0;j<cols1.size();j++){
                jb.put(title.get(j).replaceAll("\\s", ""), cols1.get(j).text());
                //System.out.print(col.text());
                System.out.println("jippko");
            }
            ja.add(jb);
            main.put("attend",ja);
        }
        out.print(main);

     
    }else{
            JSONObject failure=new JSONObject();
            failure.put("status","Wrong Credentials");
           out.println(failure);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
